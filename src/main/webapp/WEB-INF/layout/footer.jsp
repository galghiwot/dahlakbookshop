<%@ include file="/WEB-INF/layout/taglibs.jsp"%>
<div class="grid_24 footer">
    <p align="center">Copyright &copy; 2015 <a href="<c:url value="/"/>">DahlakBookShop</a>
        <br />
        Powered by <a href="https://github.com/phasenraum2010/DahlakBookShop" target="_blank">DahlakBookShop</a>
    </p>
</div>