package com.ghebits.dahlakbookshop.oodm.customer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.customer.entities.Country;
import com.ghebits.dahlakbookshop.oodm.customer.entities.Zone;

public interface ZoneRepository extends JpaRepository<Zone,Long> {

	List<Zone> findByCountry(Country country);
}
