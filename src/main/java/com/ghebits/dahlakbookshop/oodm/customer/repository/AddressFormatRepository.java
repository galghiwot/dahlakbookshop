package com.ghebits.dahlakbookshop.oodm.customer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.customer.entities.AddressFormat;

public interface AddressFormatRepository extends JpaRepository<AddressFormat,Long> {

}
