package com.ghebits.dahlakbookshop.oodm.customer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.customer.entities.Customer;

public interface CustomerRepository extends JpaRepository<Customer,Long> {

	Customer findByEmailAddress(String email);

}
