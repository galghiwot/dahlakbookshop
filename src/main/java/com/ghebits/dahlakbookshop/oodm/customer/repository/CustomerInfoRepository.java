package com.ghebits.dahlakbookshop.oodm.customer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.customer.entities.CustomerInfo;

/**
 * Created by tw on 22.12.14.
 */
public interface CustomerInfoRepository extends JpaRepository<CustomerInfo,Long> {
}
