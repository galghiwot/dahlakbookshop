package com.ghebits.dahlakbookshop.oodm.customer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.customer.entities.ProductNotification;
import com.ghebits.dahlakbookshop.oodm.customer.entities.ProductNotificationId;

/**
 * Created by tw on 23.12.14.
 */
public interface ProductNotificationRepository extends JpaRepository<ProductNotification,ProductNotificationId> {
}
