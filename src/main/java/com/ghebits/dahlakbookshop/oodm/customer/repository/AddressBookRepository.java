package com.ghebits.dahlakbookshop.oodm.customer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.customer.entities.AddressBook;
import com.ghebits.dahlakbookshop.oodm.customer.entities.Customer;

public interface AddressBookRepository extends JpaRepository<AddressBook,Long> {

	List<AddressBook> findByCustomer(Customer c);

}
