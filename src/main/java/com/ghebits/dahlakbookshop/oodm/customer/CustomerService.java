package com.ghebits.dahlakbookshop.oodm.customer;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.Language;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.Product;
import com.ghebits.dahlakbookshop.oodm.customer.entities.*;
import com.ghebits.dahlakbookshop.oodm.customer.model.CreateNewCustomerFormBean;
import com.ghebits.dahlakbookshop.oodm.customer.model.CustomerBean;
import com.ghebits.dahlakbookshop.oodm.customer.model.ProductNotificationBean;

public interface CustomerService extends UserDetailsService {

	void createNewCustomer(CreateNewCustomerFormBean createNewCustomerFormBean);

	Customer findCustomerByEmail(String string);

	void deleteCustomer(Customer c);

	void updateCustomer(Customer customer);

	List<AddressBook> findAddressBookForCustomer(Customer customer);

	AddressBook findAddressById(long addressId);

	void updateAddressBook(AddressBook persistentAddress);

	void deleteAddress(AddressBook customersAddress);

	void addAddress(AddressBook transientAddress);

	CustomerInfo findCustomerInfoByCustomer(Customer customer);

	void updateCustomerInfo(CustomerInfo myCustomerInfo);

	List<ProductNotificationBean> findAllProductNotificationsForCustomer(Customer customer, Language language);

	void addProductNotification(Product product, Customer customer);

	void updateProductNotifications(Customer customer, long[] productNotification);

    List<CustomerBean> findAllCustomers();

    int getNumberOfReviewsForCustomer(Customer customer);

    CustomerBean getCustomerById(long customerId);

}
