package com.ghebits.dahlakbookshop.oodm.customer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.customer.entities.Country;

public interface CountryRepository extends JpaRepository<Country,Long> {

}
