package com.ghebits.dahlakbookshop.oodm.customer.repository;

import com.ghebits.dahlakbookshop.oodm.customer.entities.ProductNotification;

import java.util.List;

/**
 * Created by tw on 23.12.14.
 */
public interface ProductNotificationDao {
    List<ProductNotification> findAllProductNotificationsForCustomerId(Long customerId);
}
