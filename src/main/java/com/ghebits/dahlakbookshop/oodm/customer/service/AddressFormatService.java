package com.ghebits.dahlakbookshop.oodm.customer.service;

import com.ghebits.dahlakbookshop.oodm.customer.entities.AddressFormat;

import java.util.List;

/**
 * Created by tw on 30.01.15.
 */
public interface AddressFormatService {

    List<AddressFormat> findAllAddressFormat();
}
