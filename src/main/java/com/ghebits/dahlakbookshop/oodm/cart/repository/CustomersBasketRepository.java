package com.ghebits.dahlakbookshop.oodm.cart.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.cart.entities.CustomersBasket;
import com.ghebits.dahlakbookshop.oodm.customer.entities.Customer;

public interface CustomersBasketRepository extends JpaRepository<CustomersBasket, Long> {

	CustomersBasket findByCustomerAndProductId(Customer customer,
			String productWithAttributesId);

	List<CustomersBasket> findByCustomer(Customer customer);
	

}
