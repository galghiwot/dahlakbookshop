package com.ghebits.dahlakbookshop.oodm.cart;

import java.util.Map;

import org.springframework.security.core.Authentication;
import com.ghebits.dahlakbookshop.oodm.cart.model.TransientBasket;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.Language;

public interface CartService {

	TransientBasket addProductToCart(TransientBasket transientBasket,
			long productId, Map<Long, Long> optionsAndValues,Language language);

	TransientBasket removeProductFromCart(TransientBasket transientBasket,
			long productId, Map<Long, Long> optionsAndValues, Language language);

	void update(int[] cartQuantity, TransientBasket transientBasket);

	TransientBasket polulateByPersistentBasket(TransientBasket transientBasket,Language language,Authentication auth);

}
