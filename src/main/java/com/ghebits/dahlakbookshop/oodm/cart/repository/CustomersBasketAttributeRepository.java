package com.ghebits.dahlakbookshop.oodm.cart.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.cart.entities.CustomersBasketAttribute;
import com.ghebits.dahlakbookshop.oodm.customer.entities.Customer;

public interface CustomersBasketAttributeRepository extends
		JpaRepository<CustomersBasketAttribute, Long> {

	List<CustomersBasketAttribute> findByCustomer(Customer customer);
}
