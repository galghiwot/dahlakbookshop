package com.ghebits.dahlakbookshop.oodm.catalog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.ProductOptionValue2ProductOption;

public interface ProductOptionValue2ProductOptionRepository extends 
	JpaRepository<ProductOptionValue2ProductOption,Long>{

}
