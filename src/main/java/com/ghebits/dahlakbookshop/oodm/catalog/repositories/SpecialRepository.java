package com.ghebits.dahlakbookshop.oodm.catalog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.Product;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.Special;

/**
 * Created by tw on 27.12.14.
 */
public interface SpecialRepository  extends
        JpaRepository<Special,Long> {

        Special findByProduct(Product product);
}
