package com.ghebits.dahlakbookshop.oodm.catalog.service;

import com.ghebits.dahlakbookshop.oodm.catalog.entities.Language;
import com.ghebits.dahlakbookshop.oodm.catalog.model.CategoriesBean;
import com.ghebits.dahlakbookshop.oodm.catalog.model.CategoryTree;
import com.ghebits.dahlakbookshop.oodm.catalog.model.CategoryTreeNode;
import com.ghebits.dahlakbookshop.oodm.catalog.model.ProductsByCategory;

/**
 * Created by tw on 30.01.15.
 */
public interface CategoryService {

    CategoryTree getCategoriesTree(long categoryId,Language language);

    CategoriesBean getAllCategories(Language language);

    CategoryTreeNode findCategoryById(long categoryId, Language language);

    CategoryTree getNumberOfProductsPerCategory(CategoryTree tree);

    ProductsByCategory getProductsByCategory(long categoryId,Language language);

    ProductsByCategory getProductsByCategoryAndManufacturer(long categoryId,
                                                            long manufacturerId, Language language);

}
