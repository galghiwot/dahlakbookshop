package com.ghebits.dahlakbookshop.oodm.catalog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.ManufacturerInfo;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.ManufacturerInfoId;

/**
 * Created by tw on 24.12.14.
 */
public interface ManufacturerInfoRepository extends
        JpaRepository<ManufacturerInfo,ManufacturerInfoId> {
}
