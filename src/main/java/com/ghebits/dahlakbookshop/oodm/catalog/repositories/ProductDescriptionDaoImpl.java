package com.ghebits.dahlakbookshop.oodm.catalog.repositories;

import org.springframework.stereotype.Repository;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.ProductDescription;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by tw on 09.01.15.
 */
@Repository
public class ProductDescriptionDaoImpl implements ProductDescriptionDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public ProductDescription update(ProductDescription productDescription) {
        productDescription = entityManager.merge(productDescription);
        entityManager.flush();
        entityManager.close();
        return productDescription;
    }
}
