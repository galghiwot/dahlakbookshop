package com.ghebits.dahlakbookshop.oodm.catalog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.Language;

public interface LanguageRepository extends JpaRepository<Language,Long> {

	Language findByCode(String code);
}
