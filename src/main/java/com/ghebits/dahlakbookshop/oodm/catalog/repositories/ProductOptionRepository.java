package com.ghebits.dahlakbookshop.oodm.catalog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.ProductOption;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.ProductOptionId;

public interface ProductOptionRepository extends 
	JpaRepository<ProductOption,ProductOptionId> {

}
