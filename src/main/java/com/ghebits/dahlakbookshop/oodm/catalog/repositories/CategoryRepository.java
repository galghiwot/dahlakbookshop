package com.ghebits.dahlakbookshop.oodm.catalog.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.Category;

public interface CategoryRepository extends JpaRepository<Category,Long> {
	List<Category> findByparentId(long parentId);
}
