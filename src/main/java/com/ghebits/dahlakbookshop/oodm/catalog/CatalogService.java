package com.ghebits.dahlakbookshop.oodm.catalog;

import java.util.List;


import com.ghebits.dahlakbookshop.oodm.catalog.entities.*;
import com.ghebits.dahlakbookshop.oodm.catalog.model.*;


public interface CatalogService {
	
	ProductAttributes findProductOptionsByProduct(ProductDescription product);

	List<ProductImage> findProductImages(Product product);

	int getNumberOfReviewsForProduct(Product product);

}
