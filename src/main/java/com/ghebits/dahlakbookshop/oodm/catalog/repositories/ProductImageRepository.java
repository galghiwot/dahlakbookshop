package com.ghebits.dahlakbookshop.oodm.catalog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.Product;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.ProductImage;

import java.util.List;

/**
 * Created by tw on 01.01.15.
 */
public interface ProductImageRepository extends JpaRepository<ProductImage,Long> {
    List<ProductImage> findByProductOrderBySequenceAsc(Product product);
}
