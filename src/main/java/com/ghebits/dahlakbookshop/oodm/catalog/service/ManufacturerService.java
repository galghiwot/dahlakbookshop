package com.ghebits.dahlakbookshop.oodm.catalog.service;

import com.ghebits.dahlakbookshop.oodm.catalog.entities.Language;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.Manufacturer;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.ManufacturerInfo;
import com.ghebits.dahlakbookshop.oodm.catalog.model.Manufacturers;

import java.util.List;

/**
 * Created by tw on 30.01.15.
 */
public interface ManufacturerService {

    List<Manufacturer> getAllManufacturers();

    Manufacturer getManufacturerById(long manufacturerId);

    Manufacturers findManufacturers();

    Manufacturer findManufacturerById(Long manufacturerId);

    ManufacturerInfo findManufacturerInfo(long manufacturerId, Language language);

    ManufacturerInfo clickManufacturerUrl(ManufacturerInfo manufacturerInfo);
}
