package com.ghebits.dahlakbookshop.oodm.catalog.service;

import com.ghebits.dahlakbookshop.oodm.catalog.entities.Language;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.Manufacturer;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.ProductDescription;
import com.ghebits.dahlakbookshop.oodm.catalog.model.ProductsByManufacturer;
import com.ghebits.dahlakbookshop.oodm.catalog.model.SpecialProduct;

import java.util.List;

/**
 * Created by tw on 30.01.15.
 */
public interface ProductService {

    int countProductsOfThisManufacturer(Manufacturer thisManufacturer);

    List<ProductDescription> findProductsViewed(Language language);

    List<ProductDescription> findProductsByCategoryId(long categoryId, Language language);

    void setProductActive(long productId);

    void setProductInactive(long productId);

    ProductDescription findProductById(long productId, Language language);

    List<SpecialProduct> recommenderNewProducts(Language language);

    SpecialProduct getRandomNewProduct(Language language);

    SpecialProduct viewProduct(SpecialProduct thisProduct);

    ProductsByManufacturer findProductsByManufacturer(
            Manufacturer manufacturer, Language language);

    ProductsByManufacturer findProductsByManufacturerAndCategory(
            Manufacturer manufacturer, long categoryId, Language language);
}
