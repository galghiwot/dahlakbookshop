package com.ghebits.dahlakbookshop.oodm.catalog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.ProductDescription;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.ProductDescriptionId;

public interface ProductDescriptionRepository extends 
	JpaRepository<ProductDescription,ProductDescriptionId> {
	
}
