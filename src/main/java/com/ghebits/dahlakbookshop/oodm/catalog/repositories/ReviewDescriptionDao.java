package com.ghebits.dahlakbookshop.oodm.catalog.repositories;

import com.ghebits.dahlakbookshop.oodm.catalog.entities.Language;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.Product;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.ReviewDescription;

import java.util.List;

/**
 * Created by tw on 25.12.14.
 */
public interface ReviewDescriptionDao {

    ReviewDescription create(ReviewDescription reviewDescription);

    List<ReviewDescription> findReviewsForProductAndLanguage(Product product, Language language);

    ReviewDescription findReviewsForReviewIdAndLanguage(long reviewId, Language language);
}
