package com.ghebits.dahlakbookshop.oodm.catalog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.ProductOptionValue;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.ProductOptionValueId;

public interface ProductOptionValueRepository extends 
	JpaRepository<ProductOptionValue,ProductOptionValueId>  {

}
