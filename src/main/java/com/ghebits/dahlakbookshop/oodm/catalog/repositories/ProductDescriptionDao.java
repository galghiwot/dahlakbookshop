package com.ghebits.dahlakbookshop.oodm.catalog.repositories;

import com.ghebits.dahlakbookshop.oodm.catalog.entities.ProductDescription;

/**
 * Created by tw on 09.01.15.
 */
public interface ProductDescriptionDao {

    ProductDescription update(ProductDescription productDescription);
}
