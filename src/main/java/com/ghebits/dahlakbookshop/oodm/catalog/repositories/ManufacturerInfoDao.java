package com.ghebits.dahlakbookshop.oodm.catalog.repositories;

import com.ghebits.dahlakbookshop.oodm.catalog.entities.ManufacturerInfo;

/**
 * Created by tw on 24.12.14.
 */
public interface ManufacturerInfoDao {

    ManufacturerInfo update(ManufacturerInfo manufacturerInfo);
}
