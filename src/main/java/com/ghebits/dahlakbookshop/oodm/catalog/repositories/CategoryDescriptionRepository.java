package com.ghebits.dahlakbookshop.oodm.catalog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.CategoryDescription;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.CategoryDescriptionId;

public interface CategoryDescriptionRepository extends JpaRepository<CategoryDescription,CategoryDescriptionId> {

}
