package com.ghebits.dahlakbookshop.oodm.catalog.service;

import com.ghebits.dahlakbookshop.oodm.catalog.entities.*;
import com.ghebits.dahlakbookshop.oodm.catalog.model.ReviewProduct;
import com.ghebits.dahlakbookshop.oodm.catalog.model.WriteReviewBean;
import com.ghebits.dahlakbookshop.oodm.customer.entities.Customer;

import java.util.List;

/**
 * Created by tw on 30.01.15.
 */
public interface ReviewService {

    ReviewProduct getReviewById(long reviewId, Language language);

    List<ReviewProduct> getAllReviews(Language language);

    void update(Review review);

    ReviewDescription saveReview(WriteReviewBean writeReviewBean, Product product, Customer customer, Language language);

    List<ReviewDescription> findReviewsForProduct(ProductDescription productDescription);

    ReviewDescription findReviewById(long reviewId, Language language);

    ReviewDescription getRandomReview(Language language);
}
