package com.ghebits.dahlakbookshop.oodm.catalog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.Language;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.ReviewDescription;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.ReviewDescriptionId;

import java.util.List;

/**
 * Created by tw on 25.12.14.
 */
public interface ReviewDescriptionRepository extends
        JpaRepository<ReviewDescription,ReviewDescriptionId> {

        List<ReviewDescription> findByLanguage(Language language);
}
