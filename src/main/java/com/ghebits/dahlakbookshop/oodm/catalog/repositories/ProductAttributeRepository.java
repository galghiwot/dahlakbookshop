package com.ghebits.dahlakbookshop.oodm.catalog.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.Product;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.ProductAttribute;

public interface ProductAttributeRepository extends 
	JpaRepository<ProductAttribute,Long> {
	
	List<ProductAttribute> findByProduct(Product product);
}
