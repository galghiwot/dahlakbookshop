package com.ghebits.dahlakbookshop.oodm.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.admin.entities.TaxRate;

/**
 * Created by tw on 05.01.15.
 */
public interface TaxRateRepository extends JpaRepository<TaxRate, Long> {
}
