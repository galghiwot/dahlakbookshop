package com.ghebits.dahlakbookshop.oodm.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.admin.entities.TaxZone;
import com.ghebits.dahlakbookshop.oodm.admin.entities.TaxZone2Zone;

import java.util.List;


/**
 * Created by tw on 27.01.15.
 */
public interface TaxZone2ZoneRepository extends JpaRepository<TaxZone2Zone, Long> {

    List<TaxZone2Zone> findByTaxZone(TaxZone thisTaxZone);
}
