package com.ghebits.dahlakbookshop.oodm.checkout;

import java.util.List;

import com.ghebits.dahlakbookshop.oodm.admin.model.OrderAdminBean;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.Language;
import com.ghebits.dahlakbookshop.oodm.checkout.entities.Order;
import com.ghebits.dahlakbookshop.oodm.checkout.entities.OrderStatus;
import com.ghebits.dahlakbookshop.oodm.checkout.entities.OrderStatusId;
import com.ghebits.dahlakbookshop.oodm.checkout.model.OrderHistoryBean;
import com.ghebits.dahlakbookshop.oodm.checkout.model.OrderHistoryDetailsBean;
import com.ghebits.dahlakbookshop.oodm.customer.entities.Customer;

public interface OrderService {

	List<Order> findOrdersForCustomer(Customer customer);

	OrderHistoryDetailsBean findOrderDetailsById(long orderId,Language language);

	List<OrderHistoryBean> getOrderHistoryForCustomer(Customer customer,Language language);

    List<OrderStatus> findAllOrderStatuses(Language language);

    OrderStatus findOrderStatusById(OrderStatusId ordersStatusId);

    List<OrderAdminBean> getAllOrders(Language language);

    OrderAdminBean findOrderById(long orderId, Language language);
}
