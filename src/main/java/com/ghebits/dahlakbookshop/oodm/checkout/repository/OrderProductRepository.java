package com.ghebits.dahlakbookshop.oodm.checkout.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.checkout.entities.Order;
import com.ghebits.dahlakbookshop.oodm.checkout.entities.OrderProduct;

public interface OrderProductRepository extends JpaRepository<OrderProduct, Long> {

	List<OrderProduct> findByOrder(Order order);

}
