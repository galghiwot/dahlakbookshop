package com.ghebits.dahlakbookshop.oodm.checkout.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.checkout.entities.OrderProductAttribute;

public interface OrderProductAttributeRepository extends JpaRepository<OrderProductAttribute, Long> {

}
