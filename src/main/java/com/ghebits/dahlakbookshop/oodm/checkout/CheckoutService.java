package com.ghebits.dahlakbookshop.oodm.checkout;

import com.ghebits.dahlakbookshop.oodm.cart.model.TransientBasket;
import com.ghebits.dahlakbookshop.oodm.checkout.model.AddressBean;
import com.ghebits.dahlakbookshop.oodm.checkout.model.CheckoutBean;
import com.ghebits.dahlakbookshop.oodm.customer.entities.AddressBook;
import com.ghebits.dahlakbookshop.oodm.customer.entities.Country;
import com.ghebits.dahlakbookshop.oodm.customer.entities.Customer;

public interface CheckoutService {

	AddressBean transformPersistentAddressToBean(AddressBook choosenAddress);
	
	AddressBook transformBeanToPersistentAddress(
			AddressBean newAddress, 
			Country country, 
			Customer customer);

	void placeOrder(CheckoutBean checkout, TransientBasket transientBasket,
			Customer customer);

}
