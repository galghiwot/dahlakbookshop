package com.ghebits.dahlakbookshop.oodm.checkout.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.checkout.entities.Order;
import com.ghebits.dahlakbookshop.oodm.customer.entities.Customer;

public interface OrderRepository extends JpaRepository<Order, Long> {

	List<Order> findByCustomer(Customer customer);

}
