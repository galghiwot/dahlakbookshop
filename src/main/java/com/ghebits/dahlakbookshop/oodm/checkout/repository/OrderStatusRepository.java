package com.ghebits.dahlakbookshop.oodm.checkout.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ghebits.dahlakbookshop.oodm.catalog.entities.Language;
import com.ghebits.dahlakbookshop.oodm.checkout.entities.OrderStatus;
import com.ghebits.dahlakbookshop.oodm.checkout.entities.OrderStatusId;

import java.util.List;

public interface OrderStatusRepository extends JpaRepository<OrderStatus,OrderStatusId> {

    List<OrderStatus> findByLanguage(Language language);
}
